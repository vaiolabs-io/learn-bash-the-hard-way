<!--printf techniques to parse the data-->

---

# Command Line Expansions and Data Parsing

Bash shell as a programming language has great built-in parsing of command-line arguments and the various expansions it performs on words in the line.

Then bash examines the resulting words, performing up to eight types of expansion on them as appropriate:

- Brace expansion
- Tilde expansion
- Parameter and variable expansion
- Arithmetic expansion
- Command substitution
- Word splitting
- Pathname expansion
- Process substitution


### Brace expansion

Brace expansion is a mechanism by which arbitrary strings may be generated.
Brace expansions may be nested. The results of each expanded string are not sorted; left to right order is preserved. For example:

```sh
echo a{d,c,b}e
ade ace abe
```
Brace expansion is performed before any other expansions

### Tilde expansion

If a word begins with an unquoted tilde character (`~`), all of the characters up to the first unquoted slash (or all characters, if there is no unquoted slash) are considered a tilde-prefix

- `~`  The value of $HOME 
- `~/foo`  $HOME/foo
- `~fred/foo` The subdirectory foo of the home directory of the user fred
- `~+/foo` $PWD/foo
- `~-/foo` ${OLDPWD-'~-'}/foo
- `~N`  The string that would be displayed by ‘dirs +N’
- `~+N` The string that would be displayed by ‘dirs +N’
- `~-N` The string that would be displayed by ‘dirs -N’ 

---

### Parameter and variable expansion
The ‘$’ character introduces parameter expansion, command substitution, or arithmetic expansion. The parameter name or symbol to be expanded may be enclosed in braces, which are optional but serve to protect the variable to be expanded from characters immediately following it which could be interpreted as part of the name.

- `${parameter:-word}` :If parameter is unset or null, the expansion of word is substituted. Otherwise, the value of parameter is substituted.
```sh
$ v=123
$ echo ${v-unset}
123
```
- `${parameter:=word}` :   If parameter is unset or null, the expansion of word is assigned to parameter. The value of parameter is then substituted. Positional parameters and special parameters may not be assigned to in this way.
```sh
$ var=
$ : ${var:=DEFAULT}
$ echo $var
DEFAULT
```
- `${parameter:?word}`:     If parameter is null or unset, the expansion of word (or a message to that effect if word is not present) is written to the standard error and the shell, if it is not interactive, exits. Otherwise, the value of parameter is substituted.
```sh
$ var=
$ : ${var:?var is unset or null}
bash: var: var is unset or null
```
- `${parameter:+word}` :   If parameter is null or unset, nothing is substituted, otherwise the expansion of word is substituted.
```sh
$ var=123
$ echo ${var:+var is set and not null}
var is set and not null
```
- `${parameter:offset}`
- `${parameter:offset:length}`:   This is referred to as Substring Expansion. It expands to up to length characters of the value of parameter starting at the character specified by offset. If parameter is ‘@’ or ‘*’, an indexed array subscripted by ‘@’ or ‘*’, or an associative array name, the results differ as described below. If length is omitted, it expands to the substring of the value of parameter starting at the character specified by offset and extending to the end of the value. length and offset are arithmetic expressions (see Shell Arithmetic).
If offset evaluates to a number less than zero, the value is used as an offset in characters from the end of the value of parameter. If length evaluates to a number less than zero, it is interpreted as an offset in characters from the end of the value of parameter rather than a number of characters, and the expansion is the characters between offset and that result. Note that a negative offset must be separated from the colon by at least one space to avoid being confused with the `:-` expansion.
Here are some examples illustrating substring expansion on parameters and subscripted arrays:
```sh
    $ string=01234567890abcdefgh
    $ echo ${string:7}
    7890abcdefgh
    $ echo ${string:7:0}

    $ echo ${string:7:2}
    78
    $ echo ${string:7:-2}
    7890abcdef
    $ echo ${string: -7}
    bcdefgh
    $ echo ${string: -7:0}

    $ echo ${string: -7:2}
    bc
    $ echo ${string: -7:-2}
    bcdef
```
---

```sh
    $ set -- 01234567890abcdefgh
    $ echo ${1:7}
    7890abcdefgh
    $ echo ${1:7:0}

    $ echo ${1:7:2}
    78
    $ echo ${1:7:-2}
    7890abcdef
    $ echo ${1: -7}
    bcdefgh
    $ echo ${1: -7:0}

    $ echo ${1: -7:2}
    bc
    $ echo ${1: -7:-2}
    bcdef
    $ array[0]=01234567890abcdefgh
    $ echo ${array[0]:7}
    7890abcdefgh
    $ echo ${array[0]:7:0}

    $ echo ${array[0]:7:2}
    78
    $ echo ${array[0]:7:-2}
    7890abcdef
    $ echo ${array[0]: -7}
    bcdefgh
    $ echo ${array[0]: -7:0}

    $ echo ${array[0]: -7:2}
    bc
    $ echo ${array[0]: -7:-2}
    bcdef
```

If parameter is `@` or `*`, the result is length positional parameters beginning at offset. A negative offset is taken relative to one greater than the greatest positional parameter, so an offset of -1 evaluates to the last positional parameter. It is an expansion error if length evaluates to a number less than zero. 

### Arithmetic expansion

### Command substitution

### Word splitting

### Pathname expansion

### Process substitution