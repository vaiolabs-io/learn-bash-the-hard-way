<!--*debugging the scripts in ways beyond default -x 'param'-->
# Writing Bug-Free Scripts and Debugging the Rest

## Prevention Is Better Than Cure

## Comments

## Initialization of Variables

## Function Definitions

## Runtime Configuration and Options

## Process Information

### Document Your Code

### Format Your Code Consistently

### The K.I.S.S. Principle

### Grouping Commands

### Test as You Go

## Debugging a Script