<!--*reusing, chunks of code in effective way-->
# Functions

Shell functions are a way to group commands for later execution using a single name for the group. They are executed just like a "regular" command. When the name of a shell function is used as a simple command name, the list of commands associated with that function name is executed. Shell functions are executed in the current shell context; no new process is created to interpret them.

## Definition

Functions are declared using this syntax:

```sh
fname () compound-command [ redirections ]

```
or
```sh
function fname [()] compound-command [ redirections ]
```

## Compound commands


## Getting results

## Function libraries
